terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "3.0.0"
    }
  }

  required_version = ">= 0.14"
}

provider "azurerm" {
  features {}
}

resource "azurerm_resource_group" "n3yron-terraform-state" {
  name     = "n3yron-terraform-state"
  location = "West US 2"
  #  location = "West US 2"

  tags = {
    environment = "demo"
    owner       = "oleksandr_dovnich_sre"
    dept        = "4566"
  }
}


resource "azurerm_storage_account" "n3yronterraformstate" {
  name                     = "n3yronterraformstate"
  resource_group_name      = azurerm_resource_group.n3yron-terraform-state.name
  location                 = azurerm_resource_group.n3yron-terraform-state.location
  account_tier             = "Standard"
  account_replication_type = "LRS"

  tags = {
    environment = "demo"
    owner       = "oleksandr_dovnich_sre"
    dept        = "4566"
  }
}

resource "azurerm_storage_container" "n3yron-terraform-state" {
  name                  = "vhds"
  storage_account_name  = azurerm_storage_account.n3yronterraformstate.name
  container_access_type = "private"
}

output "storage_container" {
  value = azurerm_storage_container.n3yron-terraform-state
}

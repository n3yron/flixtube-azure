# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role
resource "aws_iam_role" "eks_cluster_role" {
  name = "eks-cluster-role"
  # IAM>Roles>AWSServiceRoleForAmazonEKS
  assume_role_policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Principal": {
                "Service": "eks.amazonaws.com"
            },
            "Action": "sts:AssumeRole"
        }
    ]
}
EOF
}
# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment
resource "aws_iam_role_policy_attachment" "eks_cluster_policy" {
  role       = aws_iam_role.eks_cluster_role.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
}
# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/eks_cluster
resource "aws_eks_cluster" "eks_cluster" {
  name = var.my_cluster_name
  # Allow k8s make API calls on AWS via IAM role
  role_arn = aws_iam_role.eks_cluster_role.arn
  version  = "1.22"

  vpc_config {
    subnet_ids              = [aws_subnet.public_subnets["public_subnet_1"].id, aws_subnet.public_subnets_2["public_subnet_2"].id, aws_subnet.private_subnets["private_subnet_1"].id, aws_subnet.private_subnets["private_subnet_2"].id]
    security_group_ids      = [aws_security_group.default.id]
    endpoint_private_access = false
    endpoint_public_access  = true
  }
  depends_on = [
    aws_iam_role_policy_attachment.eks_cluster_policy,
  ]
  tags = {
    VPC_Name = var.vpc_name
    Owner    = var.owner
    Dept     = var.dept_id
  }
  count = var.deploy_private_subnets ? 1 : 0
}


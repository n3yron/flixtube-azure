#module "vpc" {
#  source  = "terraform-aws-modules/vpc/aws"
#  version = "~>3.14.2"
# insert the 23 required variables here
#}

resource "aws_vpc" "main-vpc" {
  cidr_block           = "10.0.0.0/16"
  instance_tenancy     = "default"
  enable_dns_support   = true
  enable_dns_hostnames = true
}
